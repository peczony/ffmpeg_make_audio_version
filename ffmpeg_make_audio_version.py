#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import json
import subprocess
import shutil

config_file_path = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "config.json"
)
with open(config_file_path, "r", encoding="utf8") as f:
    config = json.loads(f.read())


def ffmpeg_concat(a, b):
    with open("list.txt", "w", encoding="utf8") as f:
        f.write("\n".join(f"file '{x}'" for x in [a, b]))
    line = [
        config["ffmpeg_path"],
        "-y",
        "-f",
        "concat",
        "-safe",
        "0",
        "-i",
        "list.txt",
        "-c:a",
        "copy",
        f"output/{a}",
    ]
    subprocess.run(line, check=True, capture_output=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--extension", "-e", default="wav")
    parser.add_argument("--minute_file", "-m", default="minute.wav")
    parser.add_argument("--exclude", "-x")
    args = parser.parse_args()

    if not args.extension.startswith("."):
        args.extension = "." + args.extension

    files = [f for f in os.listdir(os.getcwd()) if f.endswith(args.extension)]
    to_process = []
    to_copy = []
    for fn in sorted(files):
        if (
            fn.startswith("q")
            and fn.endswith("_a" + args.extension)
            and (args.exclude and args.exclude not in fn)
        ):
            print(f"adding {fn} to processing")
            to_process.append(fn)
        elif fn == args.minute_file:
            print(f"minute file: {fn}")
            continue
        else:
            print(f"adding {fn} to copying")
            to_copy.append(fn)
    if not os.path.isdir("output"):
        os.mkdir("output")
    for fn in to_process:
        ffmpeg_concat(fn, args.minute_file)
    for fn in to_copy:
        shutil.copy(fn, f"output/{fn}")
    if args.extension != ".mp3":
        os.chdir("output")
        files = [fn for fn in os.listdir(os.getcwd()) if fn.endswith(args.extension)]
        for fn in sorted(files):
            new_fn = f"{os.path.splitext(fn)[0]}.mp3"
            print(f"converting {fn} -> {new_fn}...")
            subprocess.run(
                [config["ffmpeg_path"], "-y", "-i", fn, "-q:a", "6", new_fn],
                check=True,
                capture_output=True,
            )
            os.remove(fn)


if __name__ == "__main__":
    main()
